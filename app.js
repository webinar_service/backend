const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');

const indexRouter = require('./routes');
const usersRouter = require('./routes/users');
const serverRouter = require('./routes/express_server');
const loginRouter = require('./routes/auth/login');
const registrationRouter = require('./routes/auth/registration');
const fogetPwdRouter = require('./routes/auth/fogetpwd');
const validrefreshRouter = require('./routes/auth/tokenvalidation')

const app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/api', serverRouter);
app.use('/api', loginRouter);
app.use('/api', registrationRouter);
app.use('/api', fogetPwdRouter)
app.use('/api', validrefreshRouter);

// // catch 404 and forward to error handler
// app.use(function(req, res, next) {
//     next(createError(404));
//   });
  
//   // error handler
//   app.use(function(err, req, res, next) {
//     // set locals, only providing error in development
//     res.locals.message = err.message;
//     res.locals.error = req.app.get('env') === 'development' ? err : {};
  
//     // render the error page
//     res.status(err.status || 500);
//     res.render('error');
//   });

module.exports = app;
