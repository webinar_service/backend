require('dotenv').config();
const Refresh = require('../../models/Refresh');
const jwt = require('jsonwebtoken');
const getAccessToken = require('../jwt/generate')
const { genRefreshLogic } = require('../jwt/generateRefresh')
const { findUidRefresh } = require('./refreshDB');

const validation = async (req, res, next) => {
    const {access_token} = req.cookies;
    const {refreshToken} = req.body;

    const access = validateAccessToken(access_token);
    // console.log(access)
    if (!access.status) { // status: false
        const { uid, role } = access.decoded 
        const userRefresh = await findUidRefresh(uid)
        if (userRefresh) {
            const refresh = validateRefreshToken(refreshToken)
            if (!refresh.status) {
                // add redirect send to login page
                // console.log('refresh validation',refresh); //refresh validation { status: false, message: 'jwt expired' }
                return res.status(301).json({status: 'redirect to login'})
            } else {
                const { payload } = await findUidRefresh(uid)
                const compare = comparedRefreshTokens(refreshToken, payload.token)
                if (compare.status) {
                    const newAccessToken = getAccessToken({uid: uid, role: role});
                    const newRefreshToken = genRefreshLogic(payload.remember);     
                    const status = {status: 'success', role: role , refreshToken: newRefreshToken}
                    if (newAccessToken && newRefreshToken) {
                        await Refresh.findOneAndUpdate({uid: uid, token: newRefreshToken, })
                        console.log('tokens-validation successfully');
                        return res
                        .status(200)
                        .cookie('access_token', newAccessToken , {path: '/api/validate-token' , httpOnly: true})
                        .json(status)
                    } 
                }
            }
        }
    } else {
        const refresh = validateRefreshToken(refreshToken)
        if (refresh) {
            const { uid } = access.decoded
            const { payload } = await findUidRefresh(uid)
            const compare = comparedRefreshTokens(refreshToken, payload.token)
            if (compare) {     
                return res.status(200).json({status: true})
            }
        } else {
            console.log(refresh)
        }

    }
}    

const validateAccessToken = (data) => {
    return jwt.verify(data, process.env.TOKEN_SECRET, (err ,decoded) => {
        if (decoded) {
            return {status: true, decoded};
        }
        else if (err.message === 'jwt expired') {
            const payload = jwt.verify(data, process.env.TOKEN_SECRET, { ignoreExpiration: true }) 
            return {status: false, decoded: payload ,message: err.message} 
        } 
        else {
            return {status: false, message: err.message}
        }
    });
}

const validateRefreshToken = (data) => {
        
    return jwt.verify(data, process.env.TOKEN_REFRESH_SECRET, (err, decoded) => {
        if (decoded) {
            return {status: true, decoded};
        } 
        else if(err.message === 'jwt expired') {
            return {status: false, message: err.message} 
        } 
        else {
            return {status: false, message: err.message} 
        }
    });
}



const comparedRefreshTokens = (refreshClient, refreshDB) => {
    if (refreshClient === refreshDB) { 
        console.log('compared')
        return {status: true, payload: 'access'}
    } else { 
        console.log('compared is faild')
        return {status: false, payload: 'compared is faild'}
    } 
}

module.exports = {validation, validateRefreshToken}