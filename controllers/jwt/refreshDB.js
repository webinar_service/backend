const Refresh = require('../../models/Refresh');
// let data = "620124b8020da6c239033cf7";

const findUidRefresh = async (data) => {
    const result = await Refresh.findOne({uid: data});
    if (result) {
        return {status: true, payload: result}
    }
    else {
        return {status: false, payload: 'user is not defined'}
    }
}


module.exports = {findUidRefresh};



