require('dotenv').config();
const jwt = require('jsonwebtoken');

const generateAccessToken = (data) => {
    const token = jwt.sign(data, process.env.TOKEN_SECRET, {expiresIn: '60s'});
    return token;    
}

module.exports = generateAccessToken;