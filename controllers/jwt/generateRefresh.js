require('dotenv').config();
const jwt = require('jsonwebtoken');

const genRefreshLogic = (data) => {
    if (!data) { 
       return generateRefreshToken();
    } else {
        return generateRefreshTokenRemember();
    }
}

const generateRefreshToken = () => {
    const token = jwt.sign({}, process.env.TOKEN_REFRESH_SECRET, {expiresIn: '60s'});
    return token;    
}
const generateRefreshTokenRemember = () => {
    const token = jwt.sign({}, process.env.TOKEN_REFRESH_SECRET, {expiresIn: '14d'});
    return token;    
}


module.exports = {genRefreshLogic, generateRefreshToken, generateRefreshTokenRemember};
    