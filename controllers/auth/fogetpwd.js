const User = require('../../models/User');

const fogetpwd = async (form) => {
    try {
        //find email in collection "Users"
        const user = await User.find({email: form.email}).exec();
        console.log('return user from fogetpwd ', user)
        if (user.length !== 0) {
            return {status: true};
        }
        else {
            return {status: false};
        }
    } catch (err) {
        console.log(err.message)
    }
}

module.exports = fogetpwd;