const User = require('../../models/User');
const Role = require('../../models/Role');
const Refresh = require('../../models/Refresh');
const bcrypt = require('bcrypt');
const getAccessToken = require('../jwt/generate');
const { genRefreshLogic } = require('../jwt/generateRefresh');

const login = async (req, res, next) => {
    try {
        const data = req.body
        //find email in collection "Users"
        const user = await User.findOne({email: data.email});
        const checkPwd = await bcrypt.compare(data.pwd, user.password);
        if (!checkPwd) {
            throw new Error('password is not compared');   
        }
        // find user role in collection "Roles"
        const role = await Role.findOne({uid: user._id});
        //generate jwt token
        const token = getAccessToken({uid: user._id, role: role.role});
        const refreshToken = genRefreshLogic(data.remember);     
        const status = {status: 'success', password: checkPwd, role: role.role, refreshToken: refreshToken}
        if (token && refreshToken) {
            await Refresh.findOneAndUpdate({uid: user._id, token: refreshToken, })
            console.log('login is successfully');
            next();
            return res
            .status(200)
            .cookie('access_token', token, {path: '/api/validate-token' , httpOnly: true})
            .json(status)
        }
    } catch (err) {
        console.log('catch error: ', err.message)
        res.json({error: err.message})
    }
}

module.exports = login;