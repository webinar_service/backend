require('dotenv').config({path: '../../.env'});
const userModel = require('../../models/User');
const roleModel = require('../../models/Role');
const refreshModel = require('../../models/Refresh');
const bcrypt = require('bcrypt');

const registration = async (req, res, next) => {
    const data = req.body;
    const salt = await bcrypt.genSalt(Number(process.env.SALT));
    const password = await bcrypt.hash(data.pwd, salt);
    const user = new userModel;
    user.name = data.name;
    user.email = data.email;
    user.password = password;
    await user.save((err, doc) => {
        if (err) {
            return res.json({status: false, message: err.message})
        }
        if (doc) {
            addRole(doc.id)
            addRefreshId(doc.id)
            return res.json({status: 'Welcome new user', payload: {username: doc.name}}) 
        } 
    })
}

const addRole = async (id) => {
    const role = new roleModel;
    role.uid = id;
    role.role = 'user';
    await role.save();
}

const addRefreshId = async (id) => {
    const add = new refreshModel;
    add.uid = id;
    add.remember = false;
    add.token = null;
    await add.save();
}

module.exports = registration; 