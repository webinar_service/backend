const Ajv = require('ajv');
const ajv = new Ajv();

const schema = {
    type: 'object',
    properties: {
        name: {type: 'string', minLength: 1, maxLength: 50},
        email: {type:'string', minLength: 1, maxLength: 100},
        pwd: {type:'string', minLength: 4, maxLength: 50}
    },
    required: ['name', 'email', 'pwd'],
    additionalProperties: false
}

const validate = ajv.compile(schema);

const valid = async (req, res, next) => {
    const data = req.body;
    const result = await validate(data);
    if (result) {
        next();
        return console.log('validation is ok');
    }
    res.json({status: 'validation is faild', payload: validate.errors});
}

module.exports = valid;