const Ajv = require('ajv');
const ajv = new Ajv();

const schema = { 
    type: 'object',
    properties: {
        email: {type: 'string', minLength: 1}
    },
    required: ['email'],
    additionalProperties: false
}

const validate = ajv.compile(schema)

const valid = async (req, res, next) => {
    const data = req.body;
    const result = await validate(data);
    if (result) {
        next();
        return console.log('validation is ok')
    }
    res.json({status: 'validation is faild', payload: validate.errors});
}

module.exports = valid;