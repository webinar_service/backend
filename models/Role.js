const mongoose = require('mongoose');

const { Schema } = mongoose;

const roleSchema = new Schema({
    role: String,
    uid: {type: Schema.Types.ObjectId, ref: 'User'}
})

const Model = mongoose.model('Role', roleSchema);

module.exports = Model;