const mongoose = require('mongoose');

const { Schema } = mongoose;

const userSchema = new Schema({
    name: String,
    email: {type: String, required: true, unique: true},
    password: {type: String, minlength: 4, required: true},
    image: String,
    role: {type: Schema.Types.ObjectId, ref: 'Role' },
    token: {type: Schema.Types.ObjectId, ref: 'RefreshToken'}
})

const Model = mongoose.model('User', userSchema);

module.exports = Model;