const mongoose = require('mongoose');

const { Schema } = mongoose;

const refreshSchema = new Schema({
    token: String,
    remember: Boolean,
    uid: {type: Schema.Types.ObjectId, ref: 'User'}
})

const Model = mongoose.model('Refresh', refreshSchema);

module.exports = Model;