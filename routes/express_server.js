const express = require('express');
const router = express.Router();

router.get('/from-server', (req, res) => {
    res.send( {express: 'DATA FROM EXPRESS'} )
})

module.exports = router; 