const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const checkuser = require('../../controllers/auth/fogetpwd');
const validation = require('../../controllers/validation/fogerpwd');


const jsonParser = bodyParser.json();

router.post('/fogetpwd',jsonParser, validation, async (req, res, next) => {       
        const data = req.body;
        const result = await checkuser(data);
        if (result.status === true) {
            res.json({status: 'ok', payload: result});
        } else {
            res.json({status: 'user is not find'});
        }
});

module.exports = router;