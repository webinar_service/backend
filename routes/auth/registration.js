const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const registration = require('../../controllers/auth/registration');
const validation = require('../../controllers/validation/registration');

const jsonParser = bodyParser.json();

router.post('/signup', jsonParser, validation, registration);

module.exports = router;