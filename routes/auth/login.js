const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const login = require('../../controllers/auth/login');
const validation = require('../../controllers/validation/login');

const jsonParser = bodyParser.json();

router.post('/signin', jsonParser, validation, login);

module.exports = router;