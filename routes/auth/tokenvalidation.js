const express = require('express'); 
const router = express.Router();

const {validation, refresh} = require('../../controllers/jwt/validRefresh');

router.post('/validate-token', validation, async (req, res, next) => {
    res.json({status: 'tokens validation is ok'});
})


module.exports = router;