const mongoose = require('mongoose');

 const mongoDB = ('mongodb://localhost:27017/webinar_service');
 
 mongoose.connect(mongoDB);
 mongoose.Promise = global.Promise;
 const db = mongoose.connection;
 
db.on("error", console.error.bind(console, "connection error: "));
db.once("open", function () {
  console.log("Connected successfully");
}); 